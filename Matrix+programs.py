
# coding: utf-8

# In[2]:

def matrix_add(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if m1 == m2 and n1 == n2 :
        for i in range(0,m1):
            res1 = []
            for j in range(0,n1):
                res1.append(lst1[i][j]+lst2[i][j])
            res.append(res1)
    else:
        print("matrix addition not possible")
    return res


# In[4]:

matrix_add(([1,2,3,4],[4,5,6,7]),([1,2,3],[4,5,6]))


# In[6]:

matrix_add(([1,2,3],[4,5,6]),([1,2,3],[4,5,6]))


# In[11]:

def matrix_sub(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if m1 == m2 and n1 == n2 :
        for i in range(0,m1):
            res1 = []
            for j in range(0,n1):
                res1.append(lst1[i][j]-lst2[i][j])
            res.append(res1)
    else:
        print("matrix subtraction not possible")
    return res


# In[8]:

matrix_sub(([1,2,3],[4,5,6]),([1,2,3],[4,5,6]))


# In[10]:

matrix_sub(([1,2,3,4],[4,5,6,7]),([1,2,3],[4,5,6]))


# In[3]:

def matrix_mul(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if n1 == m2:
        for i in range(0,m1):
            res1 = []
            for j in range(0,n2):
                sum = 0
                for k in range(0,m2):
                    sum += (lst1[i][k]*lst2[k][j])    
                res1.append(sum)
            res.append(res1)
    else:
        print('matrix multiplication is not possible')
    return res


# In[6]:

matrix_mul(([1,2,3],[5,4,3],[2,3,1]),([1,2],[5,6],[2,3]))

