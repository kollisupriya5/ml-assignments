
# coding: utf-8

# In[3]:

1+1


# In[11]:

def sub(x):
    def square(y):
        return (x-y) ** 2
    return square
        
    


# In[13]:

sub(5)(2)


# In[14]:

def add(x,y):
    return x+y


# In[15]:

add(1,3)


# In[17]:

def mean(lst):
    sum = 0
    for e in lst:
        sum = add(sum ,e)
        result = sum/len(lst)
    return result


# In[19]:

mean([1,2,3,4,5])


# In[29]:

def variance(lst):
    res = 0
    for e in lst:
        res = res + sub(mean(lst))(e)
        result = res/len(lst)
    return result


# In[30]:

variance([1,2,3,4,5])


# In[45]:

def variance(lst):
    res =0
    for i in lst:
        res = res + sub(mean(lst))(i)
    result = res/len(lst)
    return result


# In[43]:

variance([1,2,3,4,5,6,7])


# In[40]:

def standard_deviation(lst):
    res = variance(lst)**(.5)
    return res
        


# In[41]:

standard_deviation([1,2,3,4,5,6,7,8])


# In[46]:

sorted([1,8,4,10,3,5,0])


# In[2]:

def median(lst):
    res = 0
    x = sorted(lst)
    print(x)
    z = len(x)
    if not z%2 == 0:
        return (z+1)//2
    else:
        res = (z)//2
        return (x[res-1]+x[res])/2
        


# In[3]:

median([1,3,2,4,8,7])


# In[22]:

def fab(n):
    if n == 1:
        return 1
    result = [1,1] 
    while len(result)<n:
        result.append(result[-1]+result[-2])
    return result[:n]


# In[23]:

fab(6)


# In[28]:

def perfect_numbers1(start,stop):
    for n in range(start,stop):
        sum=0
        for i in range(1,n):
            if n%i == 0:
                sum = sum+i
        if sum == n:
            print(n)


# In[29]:

perfect_numbers1(1,10)


# In[30]:

def prime_numbers(start,stop):
    for n in range(start,stop):
        k = 0
        for i in range(2,n):
            if n%i == 0:
                k = k + 1
        if k<=0:
            print(n)


# In[31]:

prime_numbers(1,10)


# In[33]:

def power(x, n):
    num = x
    if n == 0:
        return 1
    elif n==1:
        return x
    else:
        for i in range(1, n):
            x = x * num
        return x


# In[34]:

power(2,5)


# In[36]:

def no_of_digits(num):
    i=0
    temp = num
    while temp > 0:
        digit = temp % 10
        temp //= 10
        i = i+1
    return i


# In[38]:

no_of_digits(1634)


# In[39]:

def filter(pred, lst):
    filt = []
    for i in lst:
        if pred(i): 
            filt.append(i)
    return filt


# In[41]:

def armstrong(num):
    sum=0
    temp = num
    n = no_of_digits(num)
    while temp > 0:
        digit = temp % 10
        sum += power(digit, n)
        temp //= 10
    if num == sum: 
        return True
    else:
        return False


# In[47]:

armstrong(1634)


# In[52]:

def inc(x):
    return x+1
inc(1)


# In[45]:

def pipe(first, *rest):
    def piped(*args):
        result=first(*args)
        for f in rest:
            result=f(result)
        return result
    return piped


# In[58]:

pipe(inc,inc,inc,str)(100)


# In[61]:

def compose2(f,g):
    def composer(x):
        return f(g(x))
    return composer


# In[62]:

compose2(inc,inc)(100)

